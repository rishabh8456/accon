export const appName = "Accon";
import { Dimensions, Platform } from "react-native";


export const WINDOW = Dimensions.get("window");
export const screenWidth = Dimensions.get("window").width;
export const screenHeight = Dimensions.get("window").height;
export const iPhoneX = (Platform.OS === "ios" && Dimensions.get("window").height === 812 && Dimensions.get("window").width === 375);
export const SMALL_DEVICE_H = (WINDOW.height < 600);
export const iPhoneXTopMargin = 44;
export const iPhoneXBottomMargin = 34;
export const headerHeight = iPhoneX ? 180 : 160;
export const bottomTabbarHeight = iPhoneX ? 46 : 49;
export const keyboardBehaviour = (Platform.OS === "android") ? "height" : "padding";
export const keyboardOffsetScreenReg = 70;
export const iPhoneXBottomSafeAreaHeight = 34;


// --------------------------------- FontSizes For Whole App -------------------------------------
// More big new
export const font_8 = (Platform.OS == "ios") ? screenWidth * 0.0250 : screenWidth * 0.0225; // 8
export const font_9 = (Platform.OS == "ios") ? screenWidth * 0.0275 : screenWidth * 0.0250;// 9
export const font_10 = (Platform.OS == "ios") ? screenWidth * 0.0300 : screenWidth * 0.0275;// 10
export const font_11 = (Platform.OS == "ios") ? screenWidth * 0.0325 : screenWidth * 0.0300;// 11
export const font_12 = screenWidth * 0.032;// 12
export const font_13 = screenWidth * 0.0346;// 13
export const font_14 = (Platform.OS == "ios") ? screenWidth * 0.0400 : screenWidth * 0.0375;// 14
export const font_15 = screenWidth * 0.04;//15
export const font_16 = (Platform.OS == "ios") ? screenWidth * 0.0475 : screenWidth * 0.0425;// 16
export const font_17 = (Platform.OS == "ios") ? screenWidth * 0.0500 : screenWidth * 0.0450;// 17
export const font_18 = (Platform.OS == "ios") ? screenWidth * 0.048 : screenWidth * 0.0475;// 18
export const font_19 = screenWidth * 0.0475;// 19
export const font_20 = screenWidth * 0.05;// 20
export const font_22 = screenWidth * 0.055;// 22
export const font_24 = screenWidth * 0.064;// 24
// export const font_24 = screenHeight * 0.0295;// 24
export const font_26 = screenWidth * 0.0693;// 26
export const font_28 = screenWidth * 0.07;// 28
export const font_29 = screenWidth * 0.075;// 28
export const font_32 = screenWidth * 0.08;// 32
export const font_36 = screenWidth * 0.09;// 36
export const font_40 = screenWidth * 0.106; // 40

export const FONT_10 = screenHeight * 0.0162;
export const FONT_12 = screenHeight * 0.0194;
export const FONT_14 = screenHeight * 0.0227;
export const FONT_16 = screenHeight * 0.0259;
export const FONT_20 = screenHeight * 0.0324;
export const FONT_24 = screenHeight * 0.0389;

// --------------------------------- Later spacing For Whole App -------------------------------------
export const ltr_sp_013 = screenWidth * 0.000346; // 0.13 pt
export const ltr_sp_014 = screenWidth * 0.000373; // 0.14 pt
export const ltr_sp_015 = screenWidth * 0.0004; // 0.15 pt
export const ltr_sp_016 = screenWidth * 0.000426;// 0.16 pt
export const ltr_sp_024 = screenWidth * 0.00064;// 0.24 pt
export const ltr_sp_044 = screenWidth * 0.00117; // 0.44pt


// Font Family SFPro
export const SFProDisplayBold = "SFProDisplay-Bold";
export const SFProTextBold = "SFProText-Bold";
export const SFProTextHeavy = "SFProText-Heavy";
export const SFProTextMedium = "SFProText-Medium";
export const SFProTextRegular = "SFProText-Regular";
export const SFProTextSemibold = "SFProText-Semibold";
export const SFProTextLight = "SFProText-Light";
export const OswaldBold = (Platform.OS == "ios") ? "Oswald-Bold" : "Oswald-Bold";
export const SFProDisplayMedium = "SFProDisplay-Medium";















